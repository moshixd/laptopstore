const laptopsElement = document.getElementById("laptops");
const titleElement = document.getElementById("title");
const descriptionElement = document.getElementById("description");
const specsElement = document.getElementById("specs");
const priceElement = document.getElementById("price");
const stockElement = document.getElementById("stock");
const activeElement = document.getElementById("active");
const imageElement = document.getElementById("image");

let balance = 200;
let salary = 0;
let laptops = [];
let specs = [];

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
  .then((response) => response.json())
  .then((data) => (laptops = data))
  .then((laptops) => addLaptopsToSelect(laptops));

const addLaptopsToSelect = (laptops) => {
  laptops.forEach((x) => addLaptopToSelect(x));
};

const addLaptopToSelect = (laptop) => {
  const laptopElement = document.createElement("option");
  laptopElement.value = laptop.title;
  laptopElement.appendChild(document.createTextNode(laptop.title));
  laptopsElement.appendChild(laptopElement);
};

document.getElementById("balance").innerHTML = balance + " kr";
document.getElementById("salary").innerHTML = salary + " kr";

function work() {
  salary += 100;
  document.getElementById("salary").innerHTML = salary + " kr";
}

function bank() {
  balance += salary;
  document.getElementById("balance").innerHTML = balance + " kr";
  salary = 0;
  document.getElementById("salary").innerHTML = salary + " kr";
}

function getLoan() {
  balance = document.getElementById("balance").innerHTML = balance;

  loanAmount = prompt("please enter the amount to loan: ", "Amount");
  balance += loanAmount;
  return (document.getElementById("balance").innerHTML = balance + " kr");
}
