const laptopsElement = document.getElementById("laptops");
const titleElement = document.getElementById("title");
const descriptionElement = document.getElementById("description");
const specsElement = document.getElementById("specs");
const priceElement = document.getElementById("price");
const stockElement = document.getElementById("stock");
const activeElement = document.getElementById("active");
const imageElement = document.getElementById("image");

let balance = 200;
let loan = 0;
let salary = 0;
let laptops = [];

let balanceEl = document.getElementById("balance");
balanceEl.innerHTML = balance + " kr";
let loanEl = document.getElementById("loan");
loanEl.innerHTML = loan + " kr";
let salaryEl = document.getElementById("salary");
salaryEl.innerHTML = salary + " kr";

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
  .then((response) => response.json())
  .then((data) => (laptops = data))
  .then((laptops) => addLaptopsToSelect(laptops));

const addLaptopsToSelect = (laptops) => {
  laptops.forEach((x) => addLaptopToSelect(x));
  specsElement.innerText = laptops[0];
};

const addLaptopToSelect = (laptop) => {
  const laptopElement = document.createElement("option");
  laptopElement.value = laptop.id;
  laptopElement.appendChild(document.createTextNode(laptop.title));
  laptopsElement.appendChild(laptopElement);
};

// const addLaptopToSpecs = (laptop) => {
//   const laptopElement = document.createElement("details");
//   laptopElement.value = laptop.specs;
//   laptopElement.appendChild(document.createTextNode(laptop.title));
//   laptopsElement.appendChild(laptopElement);
// };

// const handleLaptop = (e) => {
//   const selectedLaptop = laptops[e.target.selectedIndex];
//   specsElement.innerText = selectedLaptop.specs;
// };

// specsElement.addEventListener("choose laptop", handleLaptop);

// function to work and get salary, 100 kr per work.
function work() {
  salary += 100;
  salaryEl.innerHTML = salary + " kr";
}

// function to get a loan
// cant get a loan if a loan exists
// cant get a loan if loan amounts to more than double balance
function getLoan() {
  loanAmount = prompt("please enter the amount to loan: ", "Amount");
  if (loan > 0) {
    alert(
      "You already have an existing loan, please pay back fully before taking a new loan."
    );
  } else if (loanAmount > 2 * balance) {
    alert("You cannot loan an amount exceeding twice your balance");
  } else loan = parseInt(loanAmount);
  balance = balance + loan;

  return (balanceEl.innerHTML = balance + " kr")(
    (loanEl.innerHTML = loan + " kr")
  );
}
// function to deposit salary to bank account/balance
// includes splitting salary 1/10 to existing loan
function bank() {
  if (loan > 0) {
    loan -= salary * 0.1; // loan = loan - salary*.1
    loanEl.innerHTML = loan + " kr";
    salary = salary * 0.9;
    balance += salary; //balance = balance + salary
    balanceEl.innerHTML = balance + " kr";
    salary = 0;
    salaryEl.innerHTML = salary + " kr";
  } else balance += salary;
  balanceEl.innerHTML = balance + " kr";
  salary = 0;
  salaryEl.innerHTML = salary + " kr";
}

function hidden() {
  if (loan < 0) {
    document.getElementById("repay").style.visibility = "hidden";
  } else document.getElementById("repay").style.visibility = "visible";
}

function payLoan() {
  if (salary > loan) {
    loan -= salary;
    salaryEl.innerHTML = salary + " kr";
    loanEl.innerHTML = loan + " kr";
    balance += salary;
    balanceEl.innerHTML = balance + " kr";
    salary = 0;
    salaryEl.innerHTML = salary + " kr";
  } else if (salary == loan) {
    loan -= salary;
    loanEl.innerHTML = loan + " kr";
    salary = 0;
    salaryEl.innerHTML = salary + " kr";
  } else loan -= salary;
  loanEl.innerHTML = loan + " kr";
  salary = 0;
  salaryEl.innerHTML = salary + " kr";
}
