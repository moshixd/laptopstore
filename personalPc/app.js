const laptopsElement = document.getElementById("laptops");
const titleElement = document.getElementById("title");
const descriptionElement = document.getElementById("description");
const specsElement = document.getElementById("specs");
const priceElement = document.getElementById("price");
const activeElement = document.getElementById("active");
const imageElement = document.getElementById("image");

let repayEl = document.getElementById("repay");

let balance = 0;
let loan = 0;
let salary = 0;
let laptops = [];
let price = 200;

let balanceEl = document.getElementById("balance");
balanceEl.innerHTML = balance + " kr";
let loanEl = document.getElementById("loan");
loanEl.innerHTML = loan + " kr";
let salaryEl = document.getElementById("salary");
salaryEl.innerHTML = salary + " kr";
repayEl.style.display = "none";

//fetch laptops data from api for displaying laptops and getting price for buy function
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
  .then((response) => response.json())
  .then((data) => (laptops = data))
  .then((laptops) => addLaptopsToSelect(laptops));

// adding data from api to laptops
const addLaptopsToSelect = (laptops) => {
  laptops.forEach((x) => addLaptopToSelect(x));
  specsElement.innerText = laptops[0].specs;
  priceElement.innerText = laptops[0].price;
  titleElement.innerText = laptops[0].title;
  descriptionElement.innerText = laptops[0].description;
  image.setAttribute(
    `src`,
    `https://noroff-komputer-store-api.herokuapp.com/${laptops[0].image}`
  );
};

// showing titles from api on the select box
const addLaptopToSelect = (laptop) => {
  const laptopElement = document.createElement("option");
  laptopElement.value = laptop.id;
  laptopElement.appendChild(document.createTextNode(laptop.title));
  laptopsElement.appendChild(laptopElement);
};

// showing data depending on the index when selecting laptop in the lower card with price image title description
const handleLaptop = (e) => {
  const selectedLaptop = laptops[e.target.selectedIndex];
  titleElement.innerText = selectedLaptop.title;
  specsElement.innerText = selectedLaptop.specs;
  descriptionElement.innerText = selectedLaptop.description;
  priceElement.innerText = selectedLaptop.price + " kr";
  price = selectedLaptop.price;
  const image = imageElement;
  image.setAttribute(
    `src`,
    `https://noroff-komputer-store-api.herokuapp.com/${selectedLaptop.image}`
  );
};

//
laptopsElement.addEventListener("change", handleLaptop);
priceElement.addEventListener("change", handleLaptop);

// function to work and get salary, 100 kr per work.
function work() {
  salary += 100;
  salaryEl.innerHTML = salary + " kr";
}

// function to get a loan
// cant get a loan if a loan exists
// cant get a loan if loan amounts to more than double balance
function getLoan() {
  loanAmount = prompt("please enter the amount to loan: ", "Amount");
  if (loan > 0) {
    alert(
      "You already have an existing loan, please pay back fully before taking a new loan."
    );
  } else if (loanAmount > 2 * balance) {
    alert("You cannot loan an amount exceeding twice your balance");
  } else if (isNaN(loanAmount) || loanAmount < 0) {
    alert("Please enter positive digits");
  } else {
    loan = parseInt(loanAmount);
    balance = balance + loan;
    updateLoanBalance(loan, balance);
    repayEl.style.display = "block";
  }
}
// function to deposit salary to bank account/balance
// includes splitting salary 1/10 to existing loan when depositing salary to bank
// if loan is less than 1/10 of salary, pay loan and add rest to balance
// if loan doesn't exist, deposit full salary to balance
function bank() {
  loanSalary = salary * 0.1;
  bankSalary = salary * 0.9;
  if (loan <= loanSalary) {
    let rest = loanSalary - loan;
    balance += bankSalary + rest;
    salary = 0;
    loan = 0;
    repayEl.style.display = "none";
    updatePayLoanDeposit(loan, balance, salary);
  } else if (loan > loanSalary) {
    loan -= loanSalary;
    balance += bankSalary;
    salary = 0;
    updatePayLoanDeposit(loan, balance, salary);
  } else {
    balance += salary;
    salary = 0;
    updateSalaryBalance(balance, salary);
    if (loan === 0) {
      repayEl.style.display = "none";
    } else repayEl.style.display = "inline";
  }
}

// repay loan, if salary > loan, pay loan and move rest to balance, if less salary < loan, pay loan
function payLoan() {
  if (salary >= loan) {
    salary -= loan;
    loan = 0;
    balance += salary;
    salary = 0;
    updatePayLoan(balance, loan, salary);
    repayEl.style.display = "none";
  } else if (salary < loan) loan -= salary;
  salary = 0;
  updateLoanSalary(loan, salary);
}

/** buy laptop function */
function buyLaptop() {
  if (balance < price) {
    alert("You cannot afford the laptop");
  } else {
    balance -= price;
    updateBalance(balance);
    alert("You are now the owner of the new laptop!");
  }
}

// for testing and seeing values on inspect
function getValues() {
  console.log(
    `
    balance: ${balance} 
    price: ${price} 
    loan: ${loan} 
    salary: ${salary}`
  );
}

function updateBalance(balance) {
  balanceEl.innerHTML = balance + " kr";
}

function updateSalary(salary) {
  salaryEl.innerHTML = salary + " kr";
}

function updateLoan(loan) {
  loanEl.innerHTML = loan + " kr";
}

function updateLoanBalance(balance, loan) {
  balanceEl.innerHTML = balance + " kr";
  loanEl.innerHTML = loan + " kr";
}

function updateSalaryBalance(salary, balance) {
  salaryEl.innerHTML = salary + " kr";
  balanceEl.innerHTML = balance + " kr";
}

function updateLoanSalary(loan, salary) {
  salaryEl.innerHTML = salary + " kr";
  loanEl.innerHTML = loan + " kr";
}

function updateBalanceSalaryLoan(balance, loan, salary) {
  balanceEl.innerHTML = balance + " kr";
  salaryEl.innerHTML = salary + " kr";
  loanEl.innerHTML = loan + " kr";
}
